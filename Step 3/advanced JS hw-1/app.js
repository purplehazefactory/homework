class Employee{
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name (){
        return this._name;
    }
    get age (){
        return this._age;
    }
    get salary (){
        return this._salary;
    }

    set name(value) {
        if (value.length < 4) {
            alert("Имя слишком короткое.");
            return;
        }
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }
    set salary(value) {
        this._salary = value;
    }
}

class Programmer extends Employee{
    constructor(lang = [], ...baseParams) {
        super(...baseParams);
        this.lang = lang;
    }
    get salary (){
        return this._salary * 3;
    }
}

const person1 = new Programmer(["ru","eng","uk"], "Mike", 30, 6000);
const person2 = new Programmer(["ru","eng","fr"], "John", 32, 4000);
const person3 = new Programmer(["ru","eng","uk", "fr"], "Sara", 27, 3000);
console.log(person1,person1.salary, person2, person2.salary, person3, person3.salary);
person1.name = "Mi";
