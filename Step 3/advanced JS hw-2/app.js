const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];
const divWrapper = document.getElementById("root");
const bookList = document.createElement("ul");
divWrapper.append(bookList);

function checkBook (item) {
    if (!item.author || !item.name || !item.price) {
        throw new SyntaxError("Данные не полные");
    }
}

function makeElem(elem){
    const bookListItem = document.createElement("li");
    bookList.append(bookListItem);
    bookListItem.innerText = `${elem.name}, автор ${elem.author}, цена ${elem.price} грн`;
}

function createUl(){
    books.forEach(function(item){
        try {
            checkBook(item);
            makeElem(item);

        }catch (e){
            function noProperty (prop){
                if(!item.hasOwnProperty(`${prop}`)){
                    console.error( `Извините, данные не полные, не указан ${prop}` );
                }
            }
            noProperty("author");
            noProperty("name");
            noProperty("price");
        }
    })
}
createUl();

