import { BASE_URL } from './constants.js';
import { VisitCardiologist, VisitDentist, VisitTherapist }
    from './childrenClasses.js'
import { $, elems } from './DOMElems.js';

export const filterCards = () =>{

$(elems.searchArea.searchButton).addEventListener('click', async () =>{
        const token = localStorage.getItem('userToken');
        try {
            fetch(`${BASE_URL}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
                .then((res) =>{
                    if (res.status ===200){
                        return res.json();
                    }
                    else{
                        throw new Error("Некорректный ответ");
                    }
                })
                .then((res) =>{
                    const examplares = res.map(el => (el.doctor ===  "cardiologist")? new VisitCardiologist(el) : (el.doctor === "therapist") ?  new VisitTherapist(el) :  new VisitDentist(el))
                    const filteredEx = examplares.filter(card => card.title.indexOf($(elems.searchArea.searchInput).value) !== -1 || card.description.indexOf($(elems.searchArea.searchInput).value)  !== -1 )
                        .filter(card =>$(elems.searchArea.statusSelector).value !== "Все" ? card.status ===  $(elems.searchArea.statusSelector).value : true)
                        .filter(card =>$(elems.searchArea.urgentlySelector).value !== "Все" ? card.status ===  $(elems.searchArea.urgentlySelector).value : true)
                    console.log(filteredEx);
                    return filteredEx

                })
                .then((res) =>{
                    const wrapper = $('.cards-list__wrapper');
                    wrapper.innerHTML = "";
                    const mainDiv = document.createElement('div');
                    mainDiv.classList.add('row', 'row__cards');
                    mainDiv.append(...res.map(rate =>{
                        const div = document.createElement('div');
                        div.classList.add('col-md-6')
                        div.innerHTML = rate.render()
                        return div
                    }))
                    wrapper.append(mainDiv);
                    wrapper.classList.add(".cards-list__wrapper")
                })
        }
        catch (err) {
            console.log(err);
        }

    });


}

