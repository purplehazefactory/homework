const createInput = () => {
    const input = document.createElement('input');
    document.body.append(input);

    const priceText = document.createElement('span');
    document.body.prepend(priceText);
    priceText.innerText = 'Price';

    input.classList.add('input-style');
    input.onfocus = function(){
        input.style.border = 'green solid 2px';
    }

    input.onkeydown = function(event) {
        if (event.key === 'Enter') {
            this.blur();
        }
    }

    input.onblur = function() {
        input.style.border = 'grey solid 2px';

        const div = document.createElement('div');
        const span = document.createElement('span');
        const x = document.createElement('img');
        x.src = 'img/Screenshot_1.png';

        // span.style.display = 'block'
        document.body.prepend(div);
        div.prepend(span);
        div.append(x);

        span.innerText = `Текущая цена: ${input.value}`;
        input.style.color = 'green';

        x.onclick = function(){
            div.remove();
            input.value = '';
        }

        if (isNaN(input.value) || input.value <= 0){
            const nan = document.createElement('p');
            document.body.append(nan);
            nan.innerText = 'Please enter correct price';

            div.remove();

            input.style.border = 'red solid 2px';

            input.onfocus = function(){
                nan.remove();
            }
        }
    }
}
createInput();