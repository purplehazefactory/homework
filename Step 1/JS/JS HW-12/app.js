const images = document.querySelectorAll('.image-to-show');
let currentImage = 0;
let imageInterval = setInterval(nextImage,3000);

function nextImage() {
    images[currentImage].className = 'image-to-show';
    currentImage = (currentImage+1)%images.length;
    images[currentImage].className = 'image-to-show showing';
}

const pauseButton = document.getElementById('pause');
const resumeButton = document.getElementById('resume');

pauseButton.onclick = function() {
        clearInterval(imageInterval);
    }
resumeButton.onclick = function() {
    imageInterval = setInterval(nextImage,3000);

};


