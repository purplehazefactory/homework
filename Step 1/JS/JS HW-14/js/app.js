$(document).ready(function () {
    $(".new-menu-item-link").each(function () {
        $(".new-menu-item-link").click(function () {
            let elementClick = $(this).attr("href");
            let destination = $(elementClick).offset().top;

            $('html').animate({scrollTop: destination}, 500);

        });
    })

    const btn = $('#button');
    $(window).scroll(function() {
        if ($(window).scrollTop() > 500) {
            btn.css("visibility", "visible");
        } else {
            btn.css("visibility", "hidden");
        }
    });
    btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
    });
    $('#button').text("Наверх");


    $("#slide-btn").on ("click", function (){
        $( "#posts" ).slideToggle( "slow", function() {
        });
    })

});