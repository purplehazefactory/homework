const changeThemeBtn = document.createElement("button");
const navbar = document.querySelector(".navbar");
const search = document.querySelector(".search");
const btnSub = document.querySelector(".button-subscribe");
const btnBest = document.querySelector(".button-best");

function createBtn(){
    navbar.append(changeThemeBtn);
    changeThemeBtn.classList.add("change-theme-btn");
    changeThemeBtn.innerText = "Change theme";
}
createBtn();

const changeTheme = function () {
    navbar.classList.toggle ("light-theme-navbar");
    search.classList.toggle ("light-theme-navbar");
    btnSub.classList.toggle ("light-theme-button-subscribe");
    btnBest.classList.toggle ("light-theme-button-best");

    const navbarJson = JSON.stringify(navbar.classList);
    localStorage.setItem("navbar",navbarJson);

    const searchJson = JSON.stringify(search.classList);
    localStorage.setItem("search",searchJson);

    const btnSubJson = JSON.stringify(btnSub.classList);
    localStorage.setItem("btnSub",btnSubJson);


    const btnBestJson = JSON.stringify(btnBest.classList);
    localStorage.setItem("btnBest",btnBestJson);
    }

const saveNav = JSON.parse(localStorage.getItem("navbar"));

const saveSearch = JSON.parse(localStorage.getItem("search"));

const saveBtnSub = JSON.parse(localStorage.getItem("btnSub"));

const saveBtnBest = JSON.parse(localStorage.getItem("btnBest"));


for (let element in saveNav){
    navbar.classList.add(saveNav[element]);
}

for (let element in saveSearch){
    search.classList.add(saveSearch[element]);
}

for (let element in saveBtnSub){
    btnSub.classList.add(saveBtnSub[element]);
}

for (let element in saveBtnBest){
    btnBest.classList.add(saveBtnBest[element]);
}

changeThemeBtn.addEventListener("click",changeTheme);

