const showPass = function(){
    const pass = document.querySelector('#pass');
    const passConfirm = document.querySelector('#pass-confirm');
    const iconPass = document.querySelector('#icon-pass');
    const iconConfirm = document.querySelector('#icon-confirm');
    const iconPassVisible = document.querySelector('#icon-pass-visible');
    const iconConfirmVisible = document.querySelector('#icon-confirm-visible');

    const showPass = function(icon, inputValue, iconVis){
        icon.onclick = function (){
            inputValue.type = '';
            iconVis.style.display = 'inline';
            icon.style.display = 'none';
        }
    }
    showPass (iconPass, pass, iconPassVisible);
    showPass (iconConfirm, passConfirm, iconConfirmVisible);

    const hiddenPass = function(icon, inputValue, iconVis){
        icon.onclick = function (){
                inputValue.type = 'password';
                icon.style.display = 'none';
                iconVis.style.display = 'inline';
        }
    }
    hiddenPass (iconPassVisible, pass, iconPass);
    hiddenPass (iconConfirmVisible, passConfirm, iconConfirm);

    const button = document.querySelector('button');

    button.onclick = function () {
        if (pass.value === passConfirm.value && pass.value!== ""){
            alert('You are welcome');
        }else{
            const wrongPass = document.createElement('span');
            wrongPass.innerText = 'Нужно ввести одинаковые значения';
            wrongPass.classList.add ('wrong-pass');
            passConfirm.after(wrongPass);
        }
    }
}

showPass();