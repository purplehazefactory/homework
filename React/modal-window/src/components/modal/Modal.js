import React, {Component} from 'react';
import './modal.scss';
import '../../App';

export class Modal extends Component {

    showButtons = () => this.props.showButtons();
    closeModal = () => this.props.closeModal();
    showModal = () => this.state.showModal;

    render() {
        const {className, actions, header, text} = this.props;
        return this.showModal && (

            <div onClick={e => {e.stopPropagation() }} className={className}>
                <header>
                    <span>{header}</span>
                    <p onClick={() => {
                        this.closeModal();
                        this.showButtons();
                    }
                    }
                    >X</p>
                </header>
                <div className="modal-window__body">
                    <p>{text}</p>
                </div>
                {actions}
            </div>
        );
    }
}

export default Modal;