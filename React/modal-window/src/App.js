import React from 'react';
import Button from './components/button/Button';
import Modal from './components/modal/Modal';
import './components/button/button.scss'
import './components/modal/modal.scss'

class App extends React.Component {

    state = {
        showModal1: false,
        showModal2: false,
        showButton: true
    }

    showButtons = () => {
        this.setState({showButton: !this.state.showButton});
    }

    handleModal1 = e => {
        if (this.state.showModal1) {
            this.closeModal1();
            return;
        }
        this.setState({ showModal1: true });
        e.stopPropagation();
        document.addEventListener("click", this.closeModal1);

    };

    closeModal1 = () => {
        this.setState({ showModal1: false });
        this.showButtons();
        document.removeEventListener("click", this.closeModal1);
    };

    handleModal2 = e => {
        if (this.state.showModal2) {
            this.closeModal2();
            return;
        }
        this.setState({ showModal2: true });
        e.stopPropagation();
        document.addEventListener("click", this.closeModal2);

    };

    closeModal2 = () => {
        this.setState({ showModal2: false });
        this.showButtons();
        document.removeEventListener("click", this.closeModal2);
    };


    // handleModal = (id) => {
    //     this.setState({
    //         ...this.state,
    //         [`showModal${id}`]: !this.state[`showModal${id}`]
    //     })
    // }

    render() {
        const actionsFirst = (
        <>
            <button
                onClick={() => {
                    this.handleModal1();
                    this.showButtons();
                }
                }
            >Ok</button>
            <button
                onClick={() => {
                    this.handleModal1();
                    this.showButtons();
                }
                }
            >Cancel</button>
        </>)

        const actionsSecond = (
            <>
                <button
                    onClick={() => {
                        this.handleModal2();
                        this.showButtons();
                    }
                    }
                >Ok</button>
                <button
                    onClick={() => {
                        this.handleModal2();
                        this.showButtons();
                    }
                    }
                >Cancel</button>
            </>
        )

        return (
            <div className="App">
                {this.state.showModal1 &&
                <Modal
                    id="1"
                    header="Do you want to delete this file?"
                    text="Once you delete this file, it won't be possible to undo this action. Are you sure want to delete it?"
                    className="modal-window"
                    showButtons={this.showButtons}
                    closeModal={this.handleModal1}
                    showModal={this.state.showModal1}
                    actions = {actionsFirst} />}

                {this.state.showModal2 &&
                <Modal
                    id="2"
                    header="Do you want to save this file?"
                    text="If you dont save this file, it won't be possible to undo this action."
                    className="modal-window"
                    showButtons={this.showButtons}
                    closeModal={this.handleModal2}
                    showModal={this.state.showModal2}
                    actions = {actionsSecond}/>}

                {this.state.showButton &&
                <Button onClick={(e) => {
                    this.handleModal1(e);
                    this.showButtons(e);
                }
                }
                        className="button-first">Open first modal</Button>}
                {this.state.showButton &&
                <Button onClick={(e) => {
                    this.handleModal2(e);
                    this.showButtons(e);
                }
                }

                    className="button-second">Open second modal</Button>}
            </div>
        );
    }
}


export default App;
