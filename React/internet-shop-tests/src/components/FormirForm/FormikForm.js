import React from 'react';
import {Form, withFormik} from "formik";
import MyInput from "./MyInput";
import * as Yup from "yup";
import "./formikForm.scss"

const schema = Yup.object().shape({
    name: Yup.string().required("This field is required").min(2, 'Too Short!'),
    secondName: Yup.string().required("This field is required").min(2, 'Too Short!'),
    age: Yup.number("This is not a number").required("This field is required").min(18, 'Too Young!'),
    address: Yup.string().required("This field is required").min(5, 'Too Short!'),
    tel: Yup.number("This is not a number").required("This field is required").min(0, 'Too Short!'),
});

const clearCart = () => {
    localStorage.clear()
}

const FormikForm = (props) => {
    return (
        <Form>
            <h3>Данные пользователя</h3>
            <MyInput name="name" placeholder="First Name" type="text"/>
            <MyInput name="secondName" placeholder="Second Name" type="text"/>
            <MyInput name="age" placeholder="Age" type="number"/>
            <MyInput name="address" placeholder="Address" type="text"/>
            <MyInput name="tel" placeholder="tel" type="tel"/>
            <div>
                <button type="submit" disabled={props.isSubmitting}>Checkout</button>
            </div>
        </Form>
    )
};

export default withFormik({
    mapPropsToValues: (props) => ({
        name: "",
        secondName: "",
        age: "",
        address: "",
        tel: "",
    }),
    handleSubmit: (values, helpers) => {
        // setTimeout(() => {
        helpers.setSubmitting(false);
        helpers.resetForm();
        for (let key, i = 0; i < localStorage.length; i++) {
            key = localStorage.key(i)
            console.log('Submitting form', values, helpers, key, ':', localStorage.getItem(key));
        }
        clearCart()
        helpers.props.setRemCart()
        // }, 1000)
    },
    validationSchema: schema

})
(FormikForm);