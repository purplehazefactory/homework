import React from 'react';
import {useField} from "formik";

const MyInput = ({name, ...rest}) =>{
    const[field, fieldState, helpers] = useField(name);
  return  (
    <div>
        <input {...field} {...rest}/>
        {fieldState.error && fieldState.touched && <span className='error'>{fieldState.error}</span>}
    </div>
)
};
export default MyInput;
