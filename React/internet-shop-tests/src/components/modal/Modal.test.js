import React from "react";
import Modal from './Modal';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import thunk from 'redux-thunk';
import {Provider} from "react-redux";
import configureStore from 'redux-mock-store'

const middlewares = [thunk];

const mockStore = configureStore(middlewares);

const initialState = {
    cart: [],
    showModal1:false,
}

const store = mockStore(initialState);

test('Smoke test of Modal', () => {
    render(
        <Provider store={store}>
            <Modal/>
        </Provider>)
});

test('Click on X button in modal call functions closeModal and remOrAddFromArrCart', () => {

    const mockCloseModal = jest.fn();
    const mockRemOrAddFromArrCart = jest.fn();

    render(
        <Provider store={store}>
            <Modal closeModal={mockCloseModal} remOrAddFromArrCart={mockRemOrAddFromArrCart}/>
        </Provider>)

    const closeButton = document.getElementsByClassName("close-btn")[0];
    userEvent.click(closeButton);

    expect(mockCloseModal).toHaveBeenCalledTimes(1)
    expect(mockRemOrAddFromArrCart).toHaveBeenCalledTimes(1)
});

test('Click on wrapper in modal call functions closeModal and remOrAddFromArrCart', () => {

    const mockCloseModal = jest.fn();
    const mockRemOrAddFromArrCart = jest.fn();

    render(
        <Provider store={store}>
            <Modal closeModal={mockCloseModal} remOrAddFromArrCart={mockRemOrAddFromArrCart}/>
        </Provider>)

    const wrapper = document.getElementsByClassName("wrapper")[0];
    userEvent.click(wrapper);

    expect(mockCloseModal).toHaveBeenCalledTimes(1)
    expect(mockRemOrAddFromArrCart).toHaveBeenCalledTimes(1)
});

