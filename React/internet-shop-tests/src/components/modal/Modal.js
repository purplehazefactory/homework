import React from 'react';
import '../../App';
import './modal.scss';

const Modal = ({className, header, text, closeModal, buttons, remOrAddFromArrCart}) => {

        return (
            <div onClick={(e) => {
                closeModal(e)
                remOrAddFromArrCart(e)
            }
            }
            className="wrapper">
                <div onClick={e => {
                    e.stopPropagation()
                }} className={className}>
                    <header>
                        <span>{header}</span>
                        <p className="close-btn" onClick={(e) => {
                            closeModal(e)
                            remOrAddFromArrCart(e)
                        }
                        }
                        >X</p>
                    </header>
                    <div className="modal-window__body">
                        <p>{text}</p>
                    </div>
                    {buttons}
                </div>
            </div>
        );

}

export default Modal;