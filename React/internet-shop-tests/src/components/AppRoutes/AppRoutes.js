import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Favourites from "../Favourites/Favourites";
import Cart from "../Cart/Cart";
import Main from "../Main/Main";

const AppRoutes = ({handleModal1, closeModal1}) => (
    <Switch>
        <Redirect exact from="/" to="/main"/>
        <Route exact path="/main">
            <Main />
        </Route>

        <Route exact path="/favourites">
            <Favourites />
        </Route>

        <Route exact path="/cart">
            <Cart />
        </Route>
    </Switch>
);

export default AppRoutes;