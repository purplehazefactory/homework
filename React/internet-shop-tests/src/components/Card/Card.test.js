import React from "react";
import Card from './Card';
import {render} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import thunk from 'redux-thunk';
import {Provider} from "react-redux";
import configureStore from 'redux-mock-store'

const middlewares = [thunk];

const mockStore = configureStore(middlewares);

const initialState = {
    cart: [],
    showModal1: false,
}

const store = mockStore(initialState);

test('Smoke test of Card', () => {
    render(
        <Provider store={store}>
            <Card/>
        </Provider>)
});

test('Click on Add to cart button sent SET_SHOW_MODAL_1 and SAVE_IN_CART actions to redux store', () => {
    render(
        <Provider store={store}>
            <Card/>
        </Provider>)

    const addToCartButton = document.getElementsByClassName("button-first")[0];
    console.log(addToCartButton)

    expect(store.getActions()).toEqual([])
    userEvent.click(addToCartButton);
    expect(store.getActions()).toEqual([{"payload": true, "type": "SET_SHOW_MODAL_1"}, {
            "payload": [undefined],
            "type": "SAVE_IN_CART"
        }]
    )

});



