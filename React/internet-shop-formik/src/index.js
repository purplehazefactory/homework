import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {composeWithDevTools} from "redux-devtools-extension";

const initialState = {
    cards:[],
    favourites: [],
    cart: [],
    showModal1:false,
    showModal2:false,
    loadingData:true

}

const reducer = (state = initialState, action) =>{
    switch (action.type){
        case "SAVE_CARDS":
            return {...state, cards: action.payload}
        case 'SET_SHOW_MODAL_1':
            return {...state, showModal1:action.payload}
        case 'SET_SHOW_MODAL_2':
            return {...state, showModal2:action.payload}
        case 'SAVE_IN_CART':
            return {...state, cart:action.payload}
        // case 'SAVE_IN_FAVOURITES':
        //     return {...state, favourites:action.payload}
        default:
            return state
    }
}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
        <BrowserRouter>
            <App/>
        </BrowserRouter>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
