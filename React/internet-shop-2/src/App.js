import React, {useState, useEffect} from 'react';
import Cards from "./components/Cards/Cards";
import Header from "./components/Header/Header";
import AppRoutes from "./components/AppRoutes/AppRoutes";

const App = () => {
    const [favourites] = useState([])
    const [cart, setCart] = useState([])
    const [showModal1, setShowModal1] = useState(false)
    const [cards, setCards] = useState([])

    useEffect(() => {
        fetch("items.json")
            .then(data => data.json())
            .then((res) => setCards(res))

    }, [])

    const handleModal1 = e => {
        // if (showModal1) {
        //     showModal1();
        // }
        setShowModal1(true)
    };

    const closeModal1 = () => {
        setShowModal1(false)
    };

    return (
        <>
            <Header/>
            <AppRoutes cards={cards} setCards={setCards} favourites={favourites} cart={cart} setCart={setCart} showModal1={showModal1} setShowModal1={setShowModal1} handleModal1={handleModal1} closeModal1={closeModal1}/>
        </>
    );
}
export default App;

