import React, {useState, useEffect} from 'react';
import Cards from "../Cards/Cards";

const Main = ({favourites, cart, showModal1, setShowModal1, handleModal1, closeModal1, cards, setCart}) => {

    return (
        <>
            <div className="App">
                <Cards
                    cards={cards}
                    handleModal1={handleModal1}
                    showModal1={showModal1}
                    closeModal1={closeModal1}
                    favourites={favourites}
                    cart={cart}
                    setCart={setCart}
                />

            </div>
        </>
    );

}


export default Main;