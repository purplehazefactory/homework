import React from 'react';
import Card from "../Card/Card";
import './cards.scss';
import {useState} from "react";

const Cards = ({cards, handleModal1, favourites, showModal1, closeModal1, cart, setCart }) => {
    const [remFav, setRemFav] = useState(true)

    const cardsList = cards.map((card, index) =>
        <Card
            favourites={favourites}
            handleModal1={handleModal1}
            showModal1={showModal1}
            closeModal1={closeModal1}
            key={index}
            srcImg={card.imageSrc}
            cardTitle={card.name}
            price={card.price}
            article={card.article}
            color={card.color}
            cart={cart}
            setRemFav={setRemFav}
            cards={cards}
            setCart={setCart}/>)
    return (
        <div className="cards">
            {cardsList}
        </div>
    );

}

export default Cards;