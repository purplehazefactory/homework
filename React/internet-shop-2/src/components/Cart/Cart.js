import React, {useEffect} from 'react';
import {useState} from "react";
import Card from "../Card/Card";
import "./cart.scss"

const Cart = ({handleModal1, favourites, showModal1, closeModal1, cards, cart, setCart}) => {

    const [remCart, setRemCart] = useState(true)
    const [showModal2, setShowModal2] = useState(false)

    const handleModal2 = () => {
        setShowModal2(true)
    };

    const closeModal2 = () => {
        setShowModal2(false)
    }

    const delFromCart = <p className="cart-del-btn">x</p>

    useEffect(()=>{
        if((localStorage.getItem("cart:"))){
        setCart(localStorage.getItem("cart:").split(","))
    }},[])

    const cartList = cards.map((card, index) =>
        localStorage.getItem("cart:") ?
        localStorage.getItem("cart:").includes(card.article)?<Card
            favourites={favourites}
            handleModal1={handleModal1}
            showModal1={showModal1}
            closeModal1={closeModal1}
            key={index}
            srcImg={card.imageSrc}
            cardTitle={card.name}
            price={card.price}
            article={card.article}
            color={card.color}
            cart={cart}
            delFromCart={delFromCart}
            setRemCart={setRemCart}
            showModal2={showModal2}
            setShowModal2={setShowModal2}
            handleModal2={handleModal2}
            closeModal2={closeModal2}
        />:false
            :false
    )
    return (
        <div className="cards">
            {cartList}
        </div>
    )
};


export default Cart;