import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Favourites from "../Favourites/Favourites";
import Cart from "../Cart/Cart";
import Main from "../Main/Main";

const AppRoutes = ({favourites, cart, setCart, showModal1, handleModal1, setShowModal1, closeModal1, cards, setCards}) => (
    <Switch>
        <Redirect exact from="/" to="/main"/>
        <Route exact path="/main">
            <Main cards={cards} setCards={setCards} favourites={favourites} cart={cart} setCart={setCart} showModal1={showModal1} handleModal1={handleModal1} setShowModal1={setShowModal1} closeModal1={closeModal1}/>
        </Route>

        <Route exact path="/favourites">
            <Favourites cards={cards} setCards={setCards}  favourites={favourites} cart={cart} showModal1={showModal1} handleModal1={handleModal1} setShowModal1={setShowModal1} closeModal1={closeModal1}/>
        </Route>

        <Route exact path="/cart">
            <Cart cards={cards} setCards={setCards} favourites={favourites} cart={cart} setCart={setCart} showModal1={showModal1} handleModal1={handleModal1} setShowModal1={setShowModal1} closeModal1={closeModal1}/>
        </Route>
    </Switch>
);

export default AppRoutes;