import React from 'react';
import {useState} from "react";
import Card from "../Card/Card";
import {useEffect} from "react";

const Favourites = ({handleModal1, favourites, showModal1, closeModal1, cart, cards}) => {

    const [remFav, setRemFav] = useState(true)
    const favouritesList = cards.map((card, index) =>
        localStorage.getItem("favourites:") ?
            localStorage.getItem("favourites:").includes(card.article) ? <Card
                favourites={favourites}
                handleModal1={handleModal1}
                showModal1={showModal1}
                closeModal1={closeModal1}
                key={index}
                srcImg={card.imageSrc}
                cardTitle={card.name}
                price={card.price}
                article={card.article}
                color={card.color}
                cart={cart}
                setRemFav={setRemFav}

            /> : false
            :false
    )
    return (
        <div className="cards">
            {favouritesList}
        </div>
    )
};


export default Favourites;