import React, {Component} from 'react';
import '../../App';
import './modal.scss';
export class Modal extends Component {

    render() {
        const {className, header, text, closeModal} = this.props;
        return (
            <div className = "wrapper">
                <div onClick={e => {
                    e.stopPropagation()
                }} className={className}>
                    <header>
                        <span>{header}</span>
                        <p onClick={(e) => {
                            closeModal(e);
                        }
                        }
                        >X</p>
                    </header>
                    <div className="modal-window__body">
                        <p>{text}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modal;