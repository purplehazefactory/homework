import React, {Component} from 'react';
import Card from "../Card/Card";
import './cards.scss';

class Cards extends Component {
    render() {
        const {cards, handleModal1, favourites, showModal1, closeModal1, cart} = this.props;
        console.log(cards)
        const cardsList = cards.map((card, index) =>
            <Card
            favourites={favourites}
            handleModal1={handleModal1}
            showModal1={showModal1}
            closeModal1={closeModal1}
            key={index}
            srcImg={card.imageSrc}
            cardTitle={card.name}
            price={card.price}
            article={card.article}
            color={card.color}
            cart={cart}/>  )
        return (
            <div className="cards">
                {cardsList}
            </div>
        );
    }
}

export default Cards;