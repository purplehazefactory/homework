import React, {Component} from 'react';
import './button.scss';

export class Button extends Component {
    render() {
        const {onClick, children, className} = this.props;
        return (
            <button onClick = {onClick} className={className}>
                {children}
            </button>
        );
    }
}

export default Button;