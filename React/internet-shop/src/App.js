import React from 'react';
import Cards from "./components/Cards/Cards";

class App extends React.Component {

    state = {
        showModal1: false,
        cards: [],
        favourites:[],
        cart:[]
    }

    componentDidMount() {
        fetch("items.json")
            .then(data => data.json())
            .then((res) => this.setState({cards: res}))

    }

    handleModal1 = e => {
        if (this.state.showModal1) {
            this.closeModal1();
        }
        this.setState({showModal1: true});
        e.stopPropagation();
        document.addEventListener("click", this.closeModal1);

    };

    closeModal1 = () => {
        this.setState({showModal1: false});
        document.removeEventListener("click", this.closeModal1);
    };

    render() {

        return (
            <div className="App">
                <Cards
                    cards={this.state.cards}
                    handleModal1 ={this.handleModal1}
                    showModal1 = {this.state.showModal1}
                    closeModal1 = {this.closeModal1}
                    favourites={this.state.favourites}
                    cart={this.state.cart}
                />

            </div>
        );
    }
}


export default App;

