import React, {useEffect} from 'react';
import Header from "./components/Header/Header";
import AppRoutes from "./components/AppRoutes/AppRoutes";
import {useDispatch} from "react-redux";

const App = () => {
    const dispatch = useDispatch()

   const loadData = () => (dispatch) => {
       fetch ("items.json")
           .then(data => data.json())
           .then((res) => dispatch({type: "SAVE_CARDS", payload: res}))
    }
    useEffect(() => {
        dispatch(loadData())
    }, [])


    return (
        <>
            <Header/>
            <AppRoutes />
        </>
    );
}
export default App;

