import React from 'react';
import {useState} from "react";
import Card from "../Card/Card";
import {useSelector} from "react-redux";

const Favourites = () => {

    const [remFav, setRemFav] = useState(true)

    const cards = useSelector(state => state.cards)
    const favourites = useSelector(state => state.favourites)

    const favouritesList = cards.map((card, index) =>
        localStorage.getItem("favourites:") ?
            localStorage.getItem("favourites:").includes(card.article) ? <Card
                favourites={favourites}
                key={index}
                srcImg={card.imageSrc}
                cardTitle={card.name}
                price={card.price}
                article={card.article}
                color={card.color}
                setRemFav={setRemFav}

            /> : false
            :false
    )
    return (
        <div className="cards">
            {favouritesList}
        </div>
    )
};


export default Favourites;