import React, {useEffect} from 'react';
import {useState} from "react";
import Card from "../Card/Card";
import "./cart.scss"
import {useDispatch, useSelector} from "react-redux";

const Cart = () => {

    const [remCart, setRemCart] = useState(true)

    const cards = useSelector(state => state.cards)
    const dispatch = useDispatch()


    const handleModal2 = () => {
        dispatch({type: "SET_SHOW_MODAL_2", payload: true})
    };

    const closeModal2 = () => {
        dispatch({type: "SET_SHOW_MODAL_2", payload: false})
    }

    const delFromCart = <p className="cart-del-btn">x</p>

    useEffect(()=>{
        if((localStorage.getItem("cart:"))){
            dispatch({type:"SAVE_IN_CART", payload:localStorage.getItem("cart:").split(",") })

    }},[])

    const cartList = cards.map((card, index) =>
        localStorage.getItem("cart:") ?
        localStorage.getItem("cart:").includes(card.article)?<Card
            key={index}
            srcImg={card.imageSrc}
            cardTitle={card.name}
            price={card.price}
            article={card.article}
            color={card.color}
            delFromCart={delFromCart}
            setRemCart={setRemCart}
            handleModal2={handleModal2}
            closeModal2={closeModal2}
        />:false
            :false
    )
    return (
        <div className="cards">
            {cartList}
        </div>
    )
};


export default Cart;