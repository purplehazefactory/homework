import React, {useState, useEffect} from 'react';
import './card.scss';
import Button from "../button/Button";
import '../button/button.scss'
import Modal from "../modal/Modal";
import {useDispatch, useSelector} from "react-redux";

const Card = ({
                  srcImg,
                  cardTitle,
                  price,
                  article,
                  color,
                  delFromCart,
                  setRemCart,
                  setRemFav,
                  handleModal2,
                  closeModal2
              }) => {
    const [inFavourite, setInFavourite] = useState(false)

    const favourites = useSelector(state => state.favourites)
    const cart = useSelector(state => state.cart)
    const showModal1 = useSelector(state => state.showModal1)
    const showModal2 = useSelector(state => state.showModal2)
    const dispatch = useDispatch()

    useEffect(() => {
        if (localStorage.hasOwnProperty("favourites:")) {
            if (localStorage.getItem("favourites:").includes(article)) {
                if (!favourites.includes(article)) {
                    favourites.push(article)
                }
                setInFavourite(!inFavourite)
            }
        }
    }, [])

    const addToFavourite = () => {
        if (!favourites.includes(article)) {
            favourites.push(article)
        }
        localStorage.setItem('favourites:', favourites);
        setInFavourite(!inFavourite)
        removeFav()
    }

    const removeFav = () => {
        if (favourites.includes(article) && inFavourite) {
            favourites.splice(favourites.indexOf(article), 1)
            console.log(favourites)
        }
        localStorage.setItem('favourites:', favourites);
        setRemFav(state => !state)
    }

    const removeFromCart = () => {

        localStorage.setItem('cart:', cart);
        setRemCart(state => !state)
    }

    const remFromArrCart2 = () => {
        console.log(cart.indexOf(article))
        if (cart.includes(article)) {
            cart.splice(cart.indexOf(article), 1)
            console.log(cart)
        }
    }

    const addToArrCart = () => {
        if (!cart.includes(article)) {
            dispatch({type:"SAVE_IN_CART", payload:[...cart, article]})
            console.log(cart)
        }
    }

    const addToArrCartFromLS = () => {
        console.log(localStorage.getItem("cart:").split(","))
        const cartArrFromLS = (localStorage.getItem("cart:").split(","))
            const arrForCart = [...cart,...cartArrFromLS]
        arrForCart.forEach((item) =>{
            if (!cart.includes(item)){
                cart.push(item)
            }
        })
        console.log(cart)
    }

    const remFromArrCart = () => {
        cart.pop()
    }

    const addToCart = () => {
        localStorage.setItem('cart:', cart);

    }

    const handleModal1 = e => {
        dispatch({type: "SET_SHOW_MODAL_1", payload: true})
    };

    const closeModal1 = () => {
        dispatch({type: "SET_SHOW_MODAL_1", payload: false})
    };

    const buttons1 = (
        <div className="modal-window__buttons">
            <button onClick={(e) => {
                addToCart(e)
                closeModal1(e)
            }
            } className="modal-window__button">Ok
            </button>
            <button onClick={(e) => {
                closeModal1(e)
                remFromArrCart(e)
            }
            } className="modal-window__button">Cancel
            </button>
        </div>
    )

    const buttons2 = (
        <div className="modal-window__buttons">
            <button onClick={(e) => {
                removeFromCart(e)
                closeModal2(e)
            }
            } className="modal-window__button">Ok
            </button>
            <button onClick={(e) => {
                closeModal2(e)
                addToArrCartFromLS(e)
            }
            } className="modal-window__button">Cancel
            </button>
        </div>
    )

    return (
        <>
            <div className="card">

                <img src={srcImg}/>
                {
                    delFromCart ?
                        <div onClick={(e) => {
                            handleModal2(e)
                            remFromArrCart2(e)
                        }
                        }
                        >{delFromCart}</div>
                        :
                        <div>
                            <svg id={cardTitle} className={inFavourite === true ? "card__img-fav" : "card__img"}
                                 onClick={addToFavourite} height="25pt" viewBox="0 -10 511.98685 511" width="25pt"
                                 xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="m510.652344 185.902344c-3.351563-10.367188-12.546875-17.730469-23.425782-18.710938l-147.773437-13.417968-58.433594-136.769532c-4.308593-10.023437-14.121093-16.511718-25.023437-16.511718s-20.714844 6.488281-25.023438 16.535156l-58.433594 136.746094-147.796874 13.417968c-10.859376 1.003906-20.03125 8.34375-23.402344 18.710938-3.371094 10.367187-.257813 21.738281 7.957031 28.90625l111.699219 97.960937-32.9375 145.089844c-2.410156 10.667969 1.730468 21.695313 10.582031 28.09375 4.757813 3.4375 10.324219 5.1875 15.9375 5.1875 4.839844 0 9.640625-1.304687 13.949219-3.882813l127.46875-76.183593 127.421875 76.183593c9.324219 5.609376 21.078125 5.097657 29.910156-1.304687 8.855469-6.417969 12.992187-17.449219 10.582031-28.09375l-32.9375-145.089844 111.699219-97.941406c8.214844-7.1875 11.351563-18.539063 7.980469-28.925781zm0 0"
                                />
                            </svg>
                        </div>
                }
                <h3 className="card-title">{cardTitle}</h3>
                <p className="card-price">{price}</p>
                <p className="card-article">{article}</p>
                <p className="card-color">{color}</p>
                {delFromCart ? false : <Button onClick={(e) => {
                    handleModal1(e);
                    addToArrCart(e)
                }
                } className="button-first">Add to cart</Button>}
            </div>

            {showModal1 &&
            <Modal
                id="1"
                header="Добавление товара в корзину"
                text="Вы уверены, что хотите добавить данный товар в корзину?"
                className="modal-window"
                closeModal={closeModal1}
                cardTitle={cardTitle}
                buttons={buttons1}
                remOrAddFromArrCart={remFromArrCart}
            />}

            {showModal2 &&
            <Modal
                id="2"
                header="Удаление товара из корзины"
                text="Вы уверены, что хотите удалить данный товар из корзины?"
                className="modal-window"
                cardTitle={cardTitle}
                buttons={buttons2}
                closeModal={closeModal2}
                remOrAddFromArrCart={addToArrCartFromLS}
            />}
        </>
    );
}

export default Card;