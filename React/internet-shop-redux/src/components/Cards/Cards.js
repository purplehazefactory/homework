import React from 'react';
import Card from "../Card/Card";
import './cards.scss';
import {useState} from "react";
import {useSelector} from "react-redux";

const Cards = ({handleModal1, closeModal1}) => {
    const [remFav, setRemFav] = useState(true)

    const cards = useSelector(state => state.cards)

    const cardsList = cards.map((card, index) =>
        <Card
            handleModal1={handleModal1}
            closeModal1={closeModal1}
            key={index}
            srcImg={card.imageSrc}
            cardTitle={card.name}
            price={card.price}
            article={card.article}
            color={card.color}
            setRemFav={setRemFav}
        />)
    return (
        <div className="cards">
            {cardsList}
        </div>
    );

}

export default Cards;