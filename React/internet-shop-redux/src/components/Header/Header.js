import React from 'react';
import './header.scss';
import {NavLink} from "react-router-dom";

const Header = (props) => (
    <header className = "header">
        <ul className = "header__menu header-menu">
            <NavLink className="header-menu__item" to="/main">Main</NavLink>
            <NavLink className="header-menu__item" to="/favourites">Favourites</NavLink>
            <NavLink className="header-menu__item" to="/cart">Cart</NavLink>
        </ul>
    </header>
);

export default Header;