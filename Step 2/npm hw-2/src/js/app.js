const showBtn = document.querySelector(".menu-btn");
const hideBtn = document.querySelector(".menu-btn__hide");
const mainMenu = document.querySelector(".menu__list");


showBtn.onclick = function() {
    mainMenu.style.display = "block";
    showBtn.style.display = "none";
    hideBtn.style.display = "block";
}

hideBtn.onclick = function() {
    mainMenu.style.display = "none";
    showBtn.style.display = "block";
    hideBtn.style.display = "none";
}
