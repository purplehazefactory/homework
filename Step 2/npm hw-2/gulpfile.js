const gulp = require('gulp');
const sass = require ('gulp-sass');
const browserSync = require ('browser-sync').create();
const cleanCSS = require('gulp-clean-css');
const clean = require('gulp-clean');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const jsMin = require('gulp-jsmin');
const imageMin = require('gulp-imagemin');
const purgeCss = require('gulp-purgecss');

gulp.task('clean', function () {
    return gulp.src('dist/*')
        .pipe(clean({ force: true }));
});

const style = () => {
    return gulp
        .src("src/scss/**/*.scss")
        .pipe(sass())
        .pipe(purgeCss({content: ['src/scss//*.scss']  }))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({"overrideBrowserslist": 'last 2 versions'}))
        .pipe(cleanCSS())
        .pipe(gulp.dest("dist/css/"))
        .pipe(browserSync.stream())
}

const scripts = () => {
    return gulp
        .src("src/js/**/*.js")
        .pipe(jsMin())
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest("dist/js/"))
        .pipe(browserSync.stream())
}

const imgs = () => {
    return gulp
        .src('./src/imgs/**')
        .pipe(imageMin())
        .pipe(gulp.dest('./dist/imgs'));
}

const html = () => {
    return gulp
        .src('./index.html')
        .pipe(gulp.dest('./dist'));
}

const watch = () => {
    browserSync.init({
        server: {
            baseDir:'./'
        }
    });
    gulp.watch('src/scss/**/*.scss', style);
    gulp.watch('src/js/**/*.js', scripts);
    gulp.watch('src/imgs/**', imgs);
    gulp.watch('*html').on('change', browserSync.reload);
}


gulp.task('build',gulp.series('clean',gulp.parallel (style, scripts, imgs, html)));
gulp.task('watch', watch);

gulp.task('dev', gulp.series([
    'build',
    'watch'
]))




